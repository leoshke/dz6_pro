package com.example.dz6_pro;

import com.example.dz6_pro.dao.BookDao;
import com.example.dz6_pro.dao.UserDao;
import com.example.dz6_pro.entity.Book;
import com.example.dz6_pro.entity.User;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.time.LocalDate;
import java.util.List;

@SpringBootApplication
public class Dz6ProApplication {

    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(Dz6ProApplication.class, args);

        UserDao userDao = applicationContext.getBean(UserDao.class);
        BookDao bookDao = applicationContext.getBean(BookDao.class);

        User user1 = new User("Ivanov", "Ivan", LocalDate.of(1992, 1, 20), "89001234567", "12345@yandex.ru", new String[]{"Азбука", "Колобок"});
        User user2 = new User("Petrov", "Petr", LocalDate.of(2000, 8, 9), "89876543210", "pochta@yandex.ru", new String[]{"Война и Мир", "Мертвые души"});
        Book book1 = new Book("Азбука");
        Book book2 = new Book("Колобок");
        Book book3 = new Book("Война и Мир");
        Book book4 = new Book("Мертвые души");

        userDao.addUser(user1);
        userDao.addUser(user2);

        bookDao.addBook(book1);
        bookDao.addBook(book2);
        bookDao.addBook(book3);
        bookDao.addBook(book4);

        User user3 = userDao.getUserByPhone("89001234567");
        List<Book> userBook = bookDao.getAllUserBooks(user3);

        System.out.println(userBook);
    }
}
