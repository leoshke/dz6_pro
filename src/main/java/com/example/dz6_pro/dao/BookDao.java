package com.example.dz6_pro.dao;

import com.example.dz6_pro.Mapper.BookRawMapper;
import com.example.dz6_pro.entity.Book;
import com.example.dz6_pro.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class BookDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public int addBook(Book book) {
        String sql = "INSERT INTO books(name) VALUES (?)";
        return jdbcTemplate.update(sql, book.getName());
    }

    public Book getBookByName(String name) {
        String sql = "SELECT * FROM books WHERE name = ?";
        return jdbcTemplate.query(sql, new BookRawMapper(), name).stream().findFirst().orElseThrow(() -> new RuntimeException("Book doesn't exist"));
    }

    public List<Book> getAllUserBooks(User user) {
        String[] userBooks = user.getBooks();
        return Arrays.stream(userBooks).map(this::getBookByName).toList();
    }
}
