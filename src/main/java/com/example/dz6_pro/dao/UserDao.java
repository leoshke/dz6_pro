package com.example.dz6_pro.dao;

import com.example.dz6_pro.Mapper.UserRowMapper;
import com.example.dz6_pro.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class UserDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    public int addUser(User user){
        String sql = "INSERT INTO users (surname, name, date_of_birth, phone, email, book_names) VALUES (?, ?, ?, ?, ?, ?)";
        return jdbcTemplate.update(sql, user.getSurname(), user.getName(), user.getBirthDate(), user.getPhone(), user.getEmail(), user.getBooks());
    }
    public User getUserByPhone(String phone){
        String sql = "SELECT * FROM users WHERE phone = ?";
        return jdbcTemplate.query(sql, new UserRowMapper(),phone).stream().findFirst().orElseThrow(() -> new RuntimeException("User doesn't exist"));
    }
}
