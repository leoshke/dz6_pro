package com.example.dz6_pro.Mapper;

import com.example.dz6_pro.entity.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserRowMapper implements RowMapper<User> {


    @Override
    public User mapRow(ResultSet rs, int rowNum) throws SQLException {
        User user = new User();
        user.setSurname(rs.getString("surname"));
        user.setName(rs.getString("name"));
        user.setPhone(rs.getString("phone"));
        user.setEmail(rs.getString("email"));
        user.setBooks((String[]) rs.getArray("book_names").getArray());
        user.setBirthDate(rs.getDate("date_of_birth").toLocalDate());
        return user;
    }
}
