package com.example.dz6_pro.Extra_tasks;

import java.util.Scanner;
import java.util.stream.IntStream;

public class Extra1 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println(isArmstrong(scan.nextInt()));
    }

    public static boolean isArmstrong(int n) {
        String temp = String.valueOf(n);
        int sum = 0;
        for (int i = 0; i < temp.length(); i++) {
            sum += Math.pow(Integer.parseInt(String.valueOf(temp.charAt(i))), temp.length());
        }
        return sum == n;
    }
}
