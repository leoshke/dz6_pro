package com.example.dz6_pro.Extra_tasks;

import java.util.stream.IntStream;

public class Extra2 {
    public static void main(String[] args) {
        int count = 2;
        for (int i = 0; i < 100; i++) {
            System.out.println(i + " " + isPrime(i));
        }
    }
    public static boolean isPrime(int a){
        if (a < 2){
            return false;
        }
        for (int i = 2; i <= Math.sqrt(a) ; i++) {
            if (a % i == 0){
                return false;
            }
        }
        return true;
    }
}
